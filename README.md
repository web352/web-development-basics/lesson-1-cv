# Overview
WIP

> Мой сайт доступен по ссылке: https://web352.gitlab.io/web-development-basics/lesson-1-cv/

# html
Результат просмотра видео [`HTML для Начинающих - Практический Курс [2021]`](https://www.youtube.com/watch?v=DOEtVdkKwcU) от ***Владилен Минин***

## Советы

Live-Editor - https://codepen.io

**vscode plugins**:
- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
- [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- [open in browser](https://marketplace.visualstudio.com/items?itemName=techer.open-in-browser)
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
> mine
- [HTML Snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets)
> [VS Code Полный Курс для JavaScript разработчиков](https://www.youtube.com/watch?v=QeUp3CahkQw) от ***Владилен Минин***

# css

- [CSS Практика и исходники тут](https://t.me/js_by_vladilen/409)
- [MDN Web Docs - CSS ](https://developer.mozilla.org/ru/docs/Learn/CSS)

### box-model в css

> Это про отступы блоков.
>
> Каждый из отступов может настраиваться отдельно.

<details>
<summary>box-model</summary>

![img](./docs/img/css-box-model.png)

</details>



```css
.aboutme {
  background: rgba(128, 128, 0, 0);
  /* padding: 20px 10px; top bottom & left right */
  padding: 20px 15px 10px 5px;  /*top right bottom left - like a clock arrow */
  margin: 20px;
  
  border: 3px solid red;
  border-bottom-style: dotted;
}
```
### Colors

[colors](https://coolors.co/) - подбирает несколько сочетающихся цветов.

<details>
<summary>coolors.co</summary>

![img](./docs/img/coolors.png)

</details>
